# Opis frameworku Angular

Angular jest to framework wspierany oraz rozwijany przez Google.
Angular jest oparty na języku typescript i pozwala na budowanie aplikacji webowych oraz mobilnych.
Rozwiązania w Angularze są bardzo zbliżone do backendowej struktury przez co jest najczęsciej wybierany przez fullstack developerów.
Środowisko Angulara jest wstępnie skonfigurowane co przyspiesza programowanie i pozwala na szybki początek pracy z frontendem.
Narzędzie wiersza poleceń (Angular CLI) pozwala na szubkie tworzenie/dodawanie komponentów oraz przeprowadzanie testów.

Rozwiązywanie problemów -
Manipulacja DOMem - pozwala na łatwe określenie warunków wyświetlania komponentów takich jak np ngIf ngFor etc
data binding - przy zmianie wartości zmiennej w pliku ts możemy natychmiastowo zobaczyć rezultat w widoku
dependency injection - pozwala na wstrzyknięcie np.: serwisu do komponentu co pozwoli na wykonywanie np.: zapytań do API żeby uzyskać dane w JSON lub innym formacie.

Jak zostało napisane w powyższym tekście Angular jest frameworkiem do budowania aplikacji internetowych. Ma on także inne zastosowania lecz najbardziej jest mi znany z aplikacji webowych oraz mobilnych.
